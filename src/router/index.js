import { createRouter, createWebHistory } from 'vue-router'

import MainView from "@/views/MainView";
import AuthView from "@/views/AuthView";
import ProfileView from "@/views/ProfileView";
import ProfileItemView from "@/views/ProfileItem";

const routes = [
  {
    path: '/',
    component: MainView
  },
  {
    path: '/authorization',
    component: AuthView
  },
  {
    path: '/profile',
    component: ProfileView
  },
  {
    path: '/profile/:id',
    component: ProfileItemView
  },
]

const router = createRouter({
  routes,
  history: createWebHistory(process.env.BASE_URL)
})

export default router
